#!python
# -*- coding: utf-8 -*-
import sys, os, re
from os.path import dirname, abspath, join
from setuptools import setup

HERE = abspath(dirname(__file__))
##readme = open(join(HERE, 'README.rst')).read()
readme = ''

package_file = open(join(HERE, 'pallet/__init__.py'), 'rU')
__version__ = re.sub(
    r".*\b__version__\s+=\s+'([^']+)'.*",
    r'\1',
    [ line.strip() for line in package_file if '__version__' in line ].pop(0)
)


setup(
    name             = "pallet",
    version          = __version__,
    description      = "A simple saveable dictionary",
    long_description = readme,
    url              = "https://gitlab.com/lucidlylogicole/pallet",
    
    author           = "lucidlylogicole",
    author_email     = "lucidlylogicole@gmail.com",
    
    packages         = ['pallet',],
    
    keywords         = ['pallet', 'storage','json', 'dict', 'mapping', 'container', 'collection'],
    classifiers      = [
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        "Programming Language :: Python :: 2.6",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        'Topic :: Software Development',
        'Topic :: Software Development :: Libraries',
        'Topic :: Utilities',
        'License :: OSI Approved :: MIT License',
    ],
    # download_url     = "https://github.com/lucidlylogicole/pallet" % __version__,
    license          = 'MIT',
)