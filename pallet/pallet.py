import os,json,datetime

def Pallet(filename, pallet_type=dict):
    '''Get Pallet storage dictionary object given the filename'''
    class PalletObject(pallet_type):
        def __init__(self, filename, *args, **kwargs):
            super(PalletObject, self).__init__()
            self.filename = filename
            self.reload()
            self.__update(*args, **kwargs)

        def __enter__(self):
            return self
        
        def __exit__(self,type,value,traceback):
            self.save()

        def clear(self):
            '''Clear all keys and values'''
            for ky in list(self.keys()):
                self.remove(ky)

        def reload(self):
            '''Reload data from file'''
            data = {}
            if os.path.exists(self.filename):
                with open(self.filename, 'r') as f:
                    data = json.load(f)
                self.__update(**data)

        def remove(self,key):
            '''Remove a key if it exists'''
            if key in self:
                self.__delitem__(key)

        def __update(self, *args, **kwargs):
            '''Update the dictionary'''
            super(PalletObject, self).update(
                *[(k, v) for k, v in args],
                **dict((k, kwargs[k]) for k in kwargs))
        
        def dumps(self,*args,**kwargs):
            '''Return json string'''
            return json.dumps({k:v for k,v in self.items() if k != 'filename'},*args,**kwargs)
        
        def save(self,*args,**kwargs):
            '''Save the dictionary'''
            with open(self.filename, 'w') as f:
                # Ignores filename key when saving
                f.write(self.dumps(*args,**kwargs))
    
    return PalletObject(filename)

def data_handler(obj):
    '''Handle types to override json saving'''
    # Handle Date types
    if isinstance(obj, datetime.datetime) or isinstance(obj, datetime.date):
        return obj.isoformat()
    else: 
        return obj