# Pallet
Pallet is a Python dictionary with a save method to make storing data easy. The files are saved in the json format, so all data in the Pallet must be json compatible.

## Examples
```python
import pallet
P = pallet.Pallet('mysettings.pref')

# Set the name and email
P['name'] = 'John Smith'

# Get the name
print(P['name']) # returns the data

# Remove the key and its data
P.remove('name')

# Set Other Values
P['age']=101 # store an integer
P['favorites']={'food':'pizza','car':'Porsche'} # Store a dictionary in a key

# Save changes (Don't forget!)
P.save()

# Use with to open file and automatically save when done
with pallet.Pallet('mysettings.pref') as P:
    P['name']='Jim'
```



## Installation

        pip install https://github.com/lucidlylogicole/pallet/archive/master.zip

## Documentation

## Pallet(path)
- **path** path to the file to save

        P = pallet.Pallet('mysettings.pref')

### save(\*args,\*\*kwargs)
Save the data.  The optional args and kwargs are passed directly to the json.dumps method

        P.save()

### remove(key)
Remove the key and the associated data. If the key does not exist, nothing happens.

        P.remove('name')

### reload()
Reload the data from the file

        P.reload()

### clear()
Clear all keys and data

        P.clear()

### Inherit Other Types
Pallet can inherit dictionary compatible object types. To specify the type:
        
        # Use an ordered dictionary
        P = pallet.Pallet('mysettings.pref', pallet_type=collections.OrderedDict)

*This has not been tested with many types, so use carefully*

## License

[MIT](license.txt)